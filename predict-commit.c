/* This program is free software. It comes without any warranty, to the extent
 * permitted by applicable law. You can redistribute it and/or modify it under
 * the terms of the Do What The Fuck You Want To Public License, Version 2, as
 * published by Sam Hocevar. See http://www.wtfpl.net/ for more details. */

#include <argp.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <threads.h>

#include <git2.h>

// For SHA1
#include <openssl/sha.h>

// For htonl
#include <arpa/inet.h>

const char *argp_program_version = "predict-commit 0.3";
const char *argp_program_bug_address
    = "https://gitlab.com/pritambaral/predict-commit";

static char doc[]
    = "Amend HEAD commit to contain its short SHA in its own message\vEvery "
      "possible short SHA is brute forced. The commit message MUST contain a "
      "string of seven consecutive '0's (i.e., the ascii code 48), or "
      "whatever seven-character string is supplied to --pattern."
      "The first occurence of such a string is replaced with the candidate "
      "short SHA, and the resulting commit hash is checked.";

// These macros exist for no good reason, except that clang-format does
// horrible, crazy things to the struct initializer below if I inline these
// strings. ¯\_(ツ)_/¯
#define FLAG_C_DOC "Threads to run concurrently. Non-positive values become 1."
#define FLAG_P_DOC "Pattern to replace with short SHA. Defaults to '0000000'"
#define FLAG_R_DOC                                                           \
  "Replacement short SHA. The pattern will be replaced with this and a new " \
  "commit will be made. If new the commit's short SHA does not match, the "  \
  "new commit will be left in the git object db (though it can still be "    \
  "cleaned up by git gc). If it matches, and if HEAD is detached to point "  \
  "to it directly, it will be updated to point to the new commit. No "       \
  "brute-force will be run."
static struct argp_option options[] = {
  {"concurrency", 'c', "NUM_CORES", 0, FLAG_C_DOC },
  { "pattern",  'p', "0000000", 0, FLAG_P_DOC },
  { "replacement",   'r', "FFFFFFF", 0, FLAG_R_DOC },
  { 0   }
};
#undef FLAG_C_DOC
#undef FLAG_P_DOC
#undef FLAG_R_DOC

static char reflog_message[]
    = "commit (amend): s/0000000/FFFFFFF/ in commit message";
static char *reflog_message_pattern     = NULL;
static char *reflog_message_replacement = NULL;

int32_t concurrency = 0;
char   *needle      = "0000000";
char   *replacement = NULL;
int     needle_offset;

#define UNUSED __attribute__((unused))

static error_t
parse_opt(int key, char *arg, struct argp_state UNUSED *state)
{
  switch (key) {
  case 'c': concurrency = atoi(arg); break;
  case 'p': needle = arg; break;
  case 'r': replacement = arg; break;
  default: return ARGP_ERR_UNKNOWN;
  }
  return 0;
}

static struct argp argp = { options, parse_opt, 0, doc };

bool
validate_args(void)
{
  if (replacement) {
    if (concurrency)
      fprintf(stderr,
          "Given concurrency (%d) will be ignored in replacement mode.\n",
          concurrency);
  }
  else {
    fprintf(stderr, "Given concurrency: %d\n", concurrency);
    concurrency = concurrency < 1 ? 1 : concurrency;
    fprintf(stderr, "Using concurrency: %d\n", concurrency);
  }

  size_t length = strlen(needle);
  if (length != 7) {
    fprintf(stderr, "--pattern must be given a string of length 7. Got %ld.\n",
        length);
    return false;
  }
  fprintf(stderr, "Using pattern: %s\n", needle);

  if (replacement) {
    length = strlen(replacement);
    if (length != 7) {
      fprintf(stderr,
          "--replacement must be given a string of length 7. Got %ld.\n",
          length);
      return false;
    }
    fprintf(stderr, "Using replacement: %s\n", replacement);
  }
  return true;
}

git_repository *repo = NULL;
git_oid         hash_to_amend;
git_commit     *commit             = NULL;
size_t          raw_commit_datalen = 0;
char           *raw_commit_data    = NULL;
git_odb        *odb                = NULL;

int
load(void)
{
  int             ret;
  git_odb_object *obj = NULL;
  char            hash_str[GIT_OID_HEXSZ + 1];

  ret = git_repository_open_ext(
      &repo, NULL, GIT_REPOSITORY_OPEN_FROM_ENV, NULL);
  if (ret) {
    fprintf(stderr, "Failed to find & open git repository.\n");
    goto exit;
  }
  fprintf(stdout, "Found and opened git repository: %s\n",
      git_repository_commondir(repo));

  ret = git_reference_name_to_id(&hash_to_amend, repo, "HEAD");
  if (ret) {
    fprintf(stderr, "Failed to resolve the HEAD ref.\n");
    goto exit;
  }
  git_oid_tostr(hash_str, sizeof(hash_str), &hash_to_amend);
  fprintf(stdout, "Found HEAD ref: %s\n", hash_str);
  ret = git_commit_lookup(&commit, repo, &hash_to_amend);
  if (ret) {
    fprintf(stderr, "Failed to get commit from hash.\n");
    goto exit;
  }
  ret = git_repository_odb(&odb, repo);
  if (ret) {
    fprintf(stderr, "Failed to get repository object database.\n");
    goto exit;
  }
  ret = git_odb_read(&obj, odb, &hash_to_amend);
  if (ret) {
    fprintf(stderr, "Failed to read commit object from repo object db.\n");
    goto exit;
  }
  raw_commit_datalen = git_odb_object_size(obj);
  raw_commit_data    = malloc(raw_commit_datalen);
  memcpy(raw_commit_data, git_odb_object_data(obj), raw_commit_datalen);

  size_t raw_header_len = strlen(git_commit_raw_header(commit));
  if (raw_header_len >= raw_commit_datalen) {
    fprintf(stderr, "The commit has no message. This will not work.\n");
    ret = 1;
    goto exit;
  }

  const char *message_raw       = git_commit_message_raw(commit);
  char       *needle_in_message = strstr(message_raw, needle);
  if (!needle_in_message) {
    fprintf(stderr, "Pattern not found in the commit message.\n");
    ret = 1;
  }

  needle_offset = raw_header_len + 1 + (needle_in_message - message_raw);
  // Git adds a newline between the header and the body of a commit obj

exit:
  if (obj) git_odb_object_free(obj);
  return ret;
}

union hashhack
{
  unsigned char sha[20];
  uint32_t      i[5];
};

typedef unsigned char id[7];

typedef struct idvec
{
  uint32_t cap;
  uint32_t sz;
  mtx_t    mtx;
  id      *ids;
} idvec;

idvec *
idvec_create(uint32_t cap)
{
  idvec *result = malloc(sizeof(idvec));
  if (!result) return NULL;
  result->cap = cap;
  result->sz  = 0;
  if (mtx_init(&result->mtx, mtx_plain) != thrd_success) goto fail;
  result->ids = malloc(sizeof(id) * cap);
  if (!result->ids) goto fail;
  return result;

fail:
  free(result);
  return NULL;
}

int
idvec_resize(idvec *this, uint32_t newcap)
{
  void *newids = realloc(this->ids, sizeof(id) * newcap);
  if (!newids) return 1;
  this->ids = newids;
  this->cap = newcap;
  return 0;
}

int
idvec_push(idvec *this, unsigned char *sha)
{
  if (mtx_lock(&this->mtx) != thrd_success) return 1;
  if (this->sz == this->cap && idvec_resize(this, this->cap * 2)) return 1;
  memcpy(this->ids + this->sz, sha, sizeof(id));
  this->sz++;
  mtx_unlock(&this->mtx);
  return 0;
}

void
idvec_destroy(idvec *this)
{
  mtx_destroy(&this->mtx);
  free(this->ids);
  free(this);
}

typedef struct thread_payload
{
  uint32_t offset;
  idvec   *ids_found;
  char    *obj;
  size_t   obj_len;
  size_t   needle_offset;
} thread_payload;

int
run_search_thread(void *arg)
{
  int ret;
  thread_payload *this = arg;
  uint32_t offset      = this->offset;
  idvec   *ids_found   = this->ids_found;
  size_t   obj_len     = this->obj_len;
  char    *obj;
  if (offset) {
    obj = malloc(obj_len);
    if (!obj) return thrd_nomem;
    memcpy(obj, this->obj, obj_len);
  }
  else
    obj = this->obj;
  char *needle       = obj + this->needle_offset;
  char  after_needle = needle[7];

  union hashhack new_hash;
  ret = 0;
  for (uint32_t i = offset; i <= 0xFFFFFFF; i += concurrency) {
    sprintf(needle, "%07x", i);
    needle[7] = after_needle;
    SHA1((unsigned char *)obj, obj_len, new_hash.sha);
    uint32_t j = htonl(new_hash.i[0]);
    if (i == j >> 4) {
      ret = idvec_push(ids_found, (unsigned char *)needle);
      if (ret) {
        ret = thrd_nomem;
        goto exit;
      }
    }
  }

exit:
  if (offset) free(obj);
  return ret;
}

int
run_search(void)
{
  char *obj        = calloc(raw_commit_datalen + 28 + 1, sizeof(char));
  char *obj_header = obj;

  int    ret = sprintf(obj_header, "commit %ld", raw_commit_datalen);
  size_t obj_header_len = ret + 1; // sprintf(3) adds a NULL terminator, which
                                   // git also uses in its hash calculation

  size_t obj_len  = obj_header_len + raw_commit_datalen;
  char  *obj_data = obj + obj_header_len;

  memcpy(obj_data, raw_commit_data, raw_commit_datalen);
  obj_data[raw_commit_datalen] = '\0';

  idvec         *ids_found = idvec_create(4);
  thrd_t         threads[concurrency];
  thread_payload thread_payloads[concurrency];

  if (!ids_found) {
    fprintf(stderr, "Failed to allocate memory of results.\n");
    ret = 1;
    goto exit;
  }

  for (uint32_t i = 0; i < concurrency; i++) {
    thread_payload *payload = thread_payloads + i;
    payload->offset         = i;
    payload->ids_found      = ids_found;
    payload->obj            = obj;
    payload->obj_len        = obj_len;
    payload->needle_offset  = obj_header_len + needle_offset;
    if (thrd_create(threads + i, run_search_thread, payload) != thrd_success) {
      fprintf(stderr, "Failed to create thread %d.\n", i + 1);
      goto exit;
    }
    else {
      fprintf(stderr, "Spawned thread %d.\n", i + 1);
    }
  }
  for (uint32_t i = 0; i < concurrency; i++) {
    int res, ret;
    ret = thrd_join(threads[i], &res);
    if (ret != thrd_success) {
      fprintf(stderr, "Joining thread %d failed for some reason.\n", i + 1);
      continue;
    }
    switch (res) {
    case 0: break;
    case thrd_nomem:
      fprintf(stderr, "Thread %d failed on allocating memory.\n", i + 1);
      break;
    default: fprintf(stderr, "Thread %d failed for some reason.\n", i + 1);
    }
  }

  ret = !ids_found->sz;
  if (ret) fprintf(stderr, "Failed to find any collisions!\n");
  else {
    fprintf(stdout, "Found collisions:\n");
    for (int i = 0; i < ids_found->sz; i++)
      fprintf(stdout, "  [%d]: %.*s\n", i, 7, ids_found->ids[i]);
  }

exit:
  free(obj);
  idvec_destroy(ids_found);
  return ret;
}

int
run_replacement(void)
{
  char *new_data = calloc(raw_commit_datalen + 1, sizeof(char));
  memcpy(new_data, raw_commit_data, raw_commit_datalen);
  memcpy(new_data + needle_offset, replacement, 7);
  git_reference *new_ref = NULL;

  git_oid new_oid;

  int ret = git_odb_write(
      &new_oid, odb, new_data, raw_commit_datalen, GIT_OBJECT_COMMIT);
  if (ret) {
    fprintf(stderr, "Failed writing new commit to git object database.\n");
    goto exit;
  }

  char new_hash[GIT_OID_HEXSZ + 1];
  git_oid_tostr(new_hash, sizeof(new_hash), &new_oid);
  if (strncmp(new_hash, replacement, 7)) {
    fprintf(stderr, "Wrote new commit, got different hash: %s vs %s.\n",
        replacement, new_hash);
    ret = 2;
    goto exit;
  }
  else
    fprintf(stdout, "Wrote new commit: %s\n", new_hash);

  if (!git_repository_head_detached(repo)) {
    fprintf(stdout, "HEAD is not DETACHED, not updating ref.\n");
    goto exit;
  }

  if (!reflog_message_pattern)
    reflog_message_pattern = strstr(reflog_message, "0000000");
  if (!reflog_message_replacement)
    reflog_message_replacement = strstr(reflog_message, "FFFFFFF");
  memcpy(reflog_message_pattern, needle, 7);
  memcpy(reflog_message_replacement, replacement, 7);
  ret = git_reference_create_matching(
      &new_ref, repo, "HEAD", &new_oid, 1, &hash_to_amend, reflog_message);
  switch (ret) {
  case 0: break;
  case GIT_EMODIFIED:
    fprintf(stderr, "HEAD has changed. Cannot replace HEAD to new commit.\n");
    ret = 1;
    goto exit;
  default:
    fprintf(stderr, "Failed to update HEAD to new commit.\n");
    ret = 1;
    goto exit;
  }

exit:
  if (new_ref) git_reference_free(new_ref);
  if (new_data) free(new_data);
  return ret;
}

int
main(int argc, char **argv)
{
  argp_parse(&argp, argc, argv, 0, 0, 0);
  if (!validate_args()) return 1;
  git_libgit2_init();
  int ret = load();
  if (ret) goto exit;
  ret = replacement ? run_replacement() : run_search();

exit:
  if (raw_commit_data) free((void *)raw_commit_data);
  if (odb) git_odb_free(odb);
  if (commit) git_commit_free(commit);
  if (repo) git_repository_free(repo);
  git_libgit2_shutdown();
  return ret;
}
