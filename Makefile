CFLAGS = -std=c18 -g -Wall -Werror -O2 $(shell pkg-config --cflags libgit2 libcrypto) -MMD -MT $(@:.c.o=.c.d) -MF $(@:.c.o=.c.d)
LDFLAGS = $(shell pkg-config --libs libgit2 libcrypto) -pthread

SRCS = predict-commit.c
OBJS = predict-commit.c.o
DEPS = predict-commit.c.d
MAIN = predict-commit

.PHONY: all clean format

all:	$(MAIN)

$(MAIN): $(OBJS)
	$(CC) -o $(MAIN) $(OBJS) $(LDFLAGS)

%.c.o:	%.c %.c.d
	$(CC) $(CFLAGS) -c $< -o $@

clean:
	$(RM) $(OBJS) $(DEPS) $(MAIN)

format: $(SRCS)
	clang-format -i $(SRCS)

$(DEPS):

include $(DEPS)
